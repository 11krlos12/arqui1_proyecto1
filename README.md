# Proyecto 1 - Arquitectura Computacional 1 
# USAC - GUATEMALA

## Simulación de carrito controlado por app en android realizada en MIT APP INVENTOR

* Circuito general de Proyecto
<div>
<p style = 'text-align:center;'>
<img src="./img/1.png" alt="JuveYell" width="1000px">
</p>
</div>


* Concepto general del proyecto 
<div>
<p style = 'text-align:center;'>
<img src="./img/2.png" alt="JuveYell" width="600px">
</p>
</div>


* Movimiento a la derecha, el cuadro en rojo es la llanta sin movimiento
<div>
<p style = 'text-align:center;'>
<img src="./img/3.png" alt="JuveYell" width="500px">
</p>
<p style = 'text-align:center;'>
<img src="./img/3.1.png" alt="JuveYell" width="500px">
</p>
</div>

* Movimiento a la izquierda, el cuadro en rojo es la llanta sin movimiento
<div>
<p style = 'text-align:center;'>
<img src="./img/4.png" alt="JuveYell" width="500px">
</p>
<p style = 'text-align:center;'>
<img src="./img/4.1.png" alt="JuveYell" width="500px">
</p>
</div>

* Movimiento hacia el frente 
<div>
<p style = 'text-align:center;'>
<img src="./img/5.PNG" alt="JuveYell" width="500px">
</p>
<p style = 'text-align:center;'>
<img src="./img/5.1.png" alt="JuveYell" width="500px">
</p>
</div>

* Auto parado o sin movimiento
<div>
<p style = 'text-align:center;'>
<img src="./img/6.png" alt="JuveYell" width="500px">
</p>
<p style = 'text-align:center;'>
<img src="./img/6.1.png" alt="JuveYell" width="500px">
</p>
</div>

* Diseño de la app 
<div>
<p style = 'text-align:center;'>
<img src="./img/7.png" alt="JuveYell" width="400px">
</p>
</div>


### Especificaciones Técnicas de los programas utilizados, con un SO W10.
* Versión utilizada de proteus
<div>
<p style = 'text-align:center;'>
<img src="./img/9.png" alt="JuveYell" width="400px">
</p>
</div>

* Version utilizada de arduino
<div>
<p style = 'text-align:center;'>
<img src="./img/8.png" alt="JuveYell" width="500px">
</p>
</div>
