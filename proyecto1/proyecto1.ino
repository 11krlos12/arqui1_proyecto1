#include <Servo.h>
#include <SoftwareSerial.h>

/*
#-----------------------------------------------#
#                 Krlos López                   #
#                  201313894                    #
#             ♡ Debian + Gnome ♡                #
#-----------------------------------------------#
*/

//***********************variables motores*********************************
int motor1 = 2;
int motor1_1 = 3;
int motor2 = 4;
int motor2_1 = 5;
int motor3 = 6;
int motor3_1 = 7;
int motor4 = 8;
int motor4_1 = 9;
//**************************************************************************
int led_izquierda = A5;
int led_derecha = A4;
int led_choque = 11;//choque
int led_reversa = 12;//led que indica que el carro va de reversa
int led_frente = 13;//led que indica que el carro va de frente

bool bDireccion = false;//reversa o de frente
int estado = 0;//recibe las entradas del celular

const float sonido = 34300.0; // Velocidad del sonido en cm/s
long distancia = -1;
int pinSensor = 10;


int periodo = 500;
unsigned long TiempoAhora = 0;
int salida = 0;

SoftwareSerial BT1(10, 11);



void setup() {
  pinMode(motor1, OUTPUT);
  pinMode(motor1_1, OUTPUT);
  pinMode(motor2, OUTPUT);
  pinMode(motor2_1, OUTPUT);

  pinMode(motor3, OUTPUT);
  pinMode(motor3_1, OUTPUT);
  pinMode(motor4, OUTPUT);
  pinMode(motor4_1, OUTPUT);

  pinMode(pinSensor, OUTPUT);


  pinMode(led_choque, OUTPUT);
  pinMode(led_reversa, OUTPUT);
  pinMode(led_frente, OUTPUT);

  pinMode(led_derecha, OUTPUT);
  pinMode(led_izquierda, OUTPUT);


  Serial.begin(9600);



  distancia = -1;
  bDireccion = false;
}

void loop() {
  delay(100);
  check();

  if (distancia > 20) {
    salida = 0;
    if (Serial.available() > 0)
    {
      estado = Serial.read();

      if (estado == '1')      //DERECHA
      {
        restablecer();
        definirDireccionEntrada(1);
        digitalWrite(led_izquierda, LOW);
        digitalWrite(led_derecha, HIGH);
      }
      else if (estado == '2') //IZQUIERDA
      {
        restablecer();
        definirDireccionEntrada(2);
        digitalWrite(led_derecha, LOW);
        digitalWrite(led_izquierda, HIGH);
      }
      else if (estado == '3') //DELANTE
      {
        bDireccion = false;
        digitalWrite(led_reversa, LOW);
        digitalWrite(led_frente, HIGH);
        restablecer();
        adelante();
      }
      else if (estado == '4') //REVERSA
      {
        bDireccion = true;
        digitalWrite(led_frente, LOW);
        digitalWrite(led_reversa, HIGH);
        reversa();
      }
      else if (estado == '5')
      {
        detenerse();
      }
    }
    pinMode(led_choque, LOW);
  }
  else if (distancia <= 20  &&  distancia > 0)
  {
    apagarLed();
    definirDireccionEntrada(3);
    salida = 1;
    digitalWrite(led_choque, HIGH);
  }

  if(millis() > TiempoAhora + periodo) {
    TiempoAhora = millis();
    Serial.print(salida); // 0, 1, 2 0-> vacio 1->chocado 2->llego
    Serial.println();
  }
}


void definirDireccionEntrada(int entrada)
{
  switch (entrada)
  {
    case 1://*******************derecha***********************
      {
        if (bDireccion) {
          derechaReversa();
        } else {
          derecha();
        }
        break;
      }


    case 2://*******************izquierda***********************
      {
        if (bDireccion) {
          izquierdaReversa();
        } else {
          izquierda();
        }
        break;
      }


    case 3://*******************detener***********************
      {
        if (bDireccion) {
          detenerseReversa();
        } else {
          detenerse();
        }
        break;
      }
  }
}

void izquierda()
{
  digitalWrite(motor1, HIGH);
  digitalWrite(motor2, LOW);
  digitalWrite(motor3, HIGH);
  digitalWrite(motor4, HIGH);
}
void izquierdaReversa()
{
  digitalWrite(motor1_1, HIGH);
  digitalWrite(motor2_1, LOW);
  digitalWrite(motor3_1, HIGH);
  digitalWrite(motor4_1, HIGH);
}

void derecha()
{
  digitalWrite(motor1, LOW);
  digitalWrite(motor2, HIGH);
  digitalWrite(motor3, HIGH);
  digitalWrite(motor4, HIGH);
}
void derechaReversa()
{
  digitalWrite(motor1_1, LOW);
  digitalWrite(motor2_1, HIGH);
  digitalWrite(motor3_1, HIGH);
  digitalWrite(motor4_1, HIGH);
}

void adelante()
{
  //APAGO PINES
  digitalWrite(led_izquierda, LOW);
  digitalWrite(led_derecha, LOW);

  //ACTIVO MOTORES
  digitalWrite(motor1, HIGH);
  digitalWrite(motor2, HIGH);
  digitalWrite(motor3, HIGH);
  digitalWrite(motor4, HIGH);
}

void reversa()
{
  //APAGO PINES
  digitalWrite(led_izquierda, LOW);
  digitalWrite(led_derecha, LOW);

  //ACTIVO MOTORES CAMBIO DE CONFIGURACION
  digitalWrite(motor1, LOW);
  digitalWrite(motor1_1, HIGH);
  digitalWrite(motor2, LOW);
  digitalWrite(motor2_1, HIGH);
  digitalWrite(motor3, LOW);
  digitalWrite(motor3_1, HIGH);
  digitalWrite(motor4, LOW);
  digitalWrite(motor4_1, HIGH);
}

void detenerse()
{
  digitalWrite(motor1, LOW);
  digitalWrite(motor2, LOW);
  digitalWrite(motor3, LOW);
  digitalWrite(motor4, LOW);
  /*hrgomez*/
  digitalWrite(motor1_1, LOW);
  digitalWrite(motor2_1, LOW);
  digitalWrite(motor3_1, LOW);
  digitalWrite(motor4_1, LOW);
  apagarLed();
}
void detenerseReversa()
{
  digitalWrite(motor1_1, LOW);
  digitalWrite(motor2_1, LOW);
  digitalWrite(motor3_1, LOW);
  digitalWrite(motor4_1, LOW);
}


void restablecer()
{
  if (bDireccion)
  {
    //cambio direcciones reversa
    digitalWrite(motor1, LOW);
    digitalWrite(motor1_1, HIGH);

    digitalWrite(motor2, LOW);
    digitalWrite(motor2_1, HIGH);

    digitalWrite(motor3, LOW);
    digitalWrite(motor3_1, HIGH);

    digitalWrite(motor4, LOW);
    digitalWrite(motor4_1, HIGH);
  }
  else //direccion al frente
  {
    digitalWrite(motor1, HIGH);
    digitalWrite(motor1_1, LOW);

    digitalWrite(motor2, HIGH);
    digitalWrite(motor2_1, LOW);

    digitalWrite(motor3, HIGH);
    digitalWrite(motor3_1, LOW);

    digitalWrite(motor4, HIGH);
    digitalWrite(motor4_1, LOW);
  }

}


long tiempo() {
  pinMode(10, OUTPUT);
  digitalWrite(10, LOW);
  delayMicroseconds(2);
  digitalWrite(10, HIGH);
  delayMicroseconds(10);
  digitalWrite(10, LOW);

  pinMode(10, INPUT);
  return pulseIn(10, HIGH);
}

void check() {
  distancia = (tiempo() * 0.000001 * sonido / 2.0) + 1;
  //Serial.println(distancia);
}

void metaLed()
{
  salida = 2;
  digitalWrite(led_izquierda, LOW);
  digitalWrite(led_derecha, LOW);
  digitalWrite(led_choque, LOW);
  digitalWrite(led_frente, LOW);
  digitalWrite(led_reversa, LOW);
  delay(50);
  digitalWrite(led_izquierda, HIGH);
  digitalWrite(led_derecha, HIGH);
  digitalWrite(led_choque, HIGH);
  digitalWrite(led_frente, HIGH);
  digitalWrite(led_reversa, HIGH);
  delay(100);
}

void apagarLed()
{
  digitalWrite(led_izquierda, LOW);
  digitalWrite(led_derecha, LOW);
  digitalWrite(led_choque, LOW);
  digitalWrite(led_frente, LOW);
  digitalWrite(led_reversa, LOW);
}
